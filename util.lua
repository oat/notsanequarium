function mix(x, y, a)
  return x * (1 - a) + y * a
end
function clamp(a, x, y)
  return math.min(math.max(a, x), y)
end
function round(x)
  if x%1 >= 0.5 then return math.ceil(x) end
  return math.floor(x)
end
function sum(tbl)
  local sum = 0
  for _,v in ipairs(tbl) do
    sum = sum + v
  end
  return sum/#tbl
end
function shrt(num)
  return math.floor(num * 10000) / 10000
end

function pointInside(px,py,x,y,w,h)
  return px > x and px < x+w and py > y and py < y+h
end

function mouseOverBox(x,y,w,h)
  return pointInside(love.mouse.getX(), love.mouse.getY(), x, y, w, h)
end

function stretchto(sprite, x, y, r, sx, sy, ox, oy)
  love.graphics.draw(sprite, x, y, r, sx/sprite:getWidth(), sy/sprite:getHeight(), ox, oy)
end
function drawcenteredquad(sprite, quad, x, y, r, sx, sy, ox, oy)
  local sw, sh = quad:getTextureDimensions()
  love.graphics.draw(sprite, quad, x - (sw * (sx or 1))/2, y - (sh * (sy or 1))/2, r, sx, sy, (ox or 0) + sw/2, (oy or 0) + sh/2)
end
function drawcentered(sprite, x, y, r, sx, sy, ox, oy)
  love.graphics.draw(sprite, x - (sprite:getWidth() * (sx or 1))/2, y - (sprite:getHeight() * (sy or 1))/2, r, sx, sy, (ox or 0) + sprite:getWidth()/2, (oy or 0) + sprite:getHeight()/2)
end

function table.deepcopy(orig)
  local orig_type = type(orig)
  local copy
  if orig_type == 'table' then
      copy = {}
      for orig_key, orig_value in next, orig, nil do
          copy[table.deepcopy(orig_key)] = table.deepcopy(orig_value)
      end
      setmetatable(copy, table.deepcopy(getmetatable(orig)))
  else -- number, string, boolean, etc
      copy = orig
  end
  return copy
end

function math.sign(a)
  if a >= 0 then return 1 else return -1 end
end

function string.starts(str, start)
  return str:sub(1, #start) == start
end

function string.ends(str, ending)
  return ending == "" or str:sub(-#ending) == ending
end