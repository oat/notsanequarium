require 'const'

local self = {}

function self.fish(x, y, type)
  type = type or 0
  local fish = {
    x = x,
    y = y,
    size = type, -- 0 for small, 1 for medium, 2 for big, 3 for king, 4 for carnivore
    eattimer = 0 + math.random() * 2, -- starts out hungry, with a 2s delay
    moneytimer = 10 + math.random() * 5, -- min 10s before it can make money
    dead = false,
    star = false,

    lifetime = 0,

    eases = {
      
    },
    render = {
      x = 0,
      y = 0,
      prevx = math.random() - 0.5,
      prevy = math.random() - 0.5,
      turn = math.random(),
      swim = 0,
      angle = 0,
      xspeed = 0,
      yspeed = 0,
      eattimer = 1,
      hungry = 0,
      deathanim = 0,
    }
  }

  for i = 1, FISH_SPLINE_QUALITY do
    table.insert(fish.eases, { -- we insert a fake ease that ends instantly and just hope the update loop replaces it with a valid, random one
      x = x,
      y = y,
      fromx = x,
      fromy = y,
      a = 1,
      speed = 1
    })
  end

  return fish
end

function self.food(x, y, type)
  local food = {
    x = x,
    y = y,
    speed = 0.2,
    time = math.random(),
    deathtimer = 0,
    type = type -- 1 = tier 1, 2 = tier 2, 3 = tier 3, 4 = star potion
  }

  return food
end

function self.money(x, y, type)
  local money = {
    x = x,
    y = y,
    speed = 0.12,
    time = math.random(),
    deathtimer = 0,
    collected = false,
    collecttimer = 0,
    type = type
  }

  return money
end

return self