function love.conf(t)
  t.version = '11.1'
  t.release = false
  t.identity = 'notsanequarium'
  t.appendidentity = true
  t.window.title = 'notsanequarium'
  t.window.resizable = true
  t.window.vsync = false
  t.window.minwidth = 640
  t.window.minheight = 480
  t.window.width = 640
  t.window.height = 480

  t.modules.physics	= false
  t.modules.joystick = false
  t.modules.video = false
  t.modules.data = false
  t.modules.math = false
  t.modules.thread = false

  -- t.window.msaa = 2
  
  t.releases = {
    title = 'Notsanequarium',
    package = 'notsanequarium',
    loveVersion = '11.1',
    version = '1.0a',
    author = 'oatmealine',
    email = 'oatmealine@disroot.org',
    description = 'A recreation of Insaniquarium by PopCap in Love2D',
    homepage = 'https://gitdab.com/oat/notsanequarium',
    identifier = 'notsanequarium',
    excludeFileList = {},
    compile = false,
    projectDirectory = '/home/oatmealine/Git/notsanequarium',
    releaseDirectory = '/home/oatmealine/Git/notsanequarium/releases',
  }
end
