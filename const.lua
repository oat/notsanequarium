ease = require 'lib.ease'

FISH_SPLINE_QUALITY = 6 -- bigger number = more lag, smaller number = more noise
FISH_RENDER_ITERS = 4 -- a bigger number allows for more smoother flipper movement and rotation, but is significantly laggier
FISH_RENDER_ITEROFF = 0.025 -- should be 1/iters*0.1, lower numbers will make movement snappier and higher numbers will make it smoother but glitchier

FISH_RENDER_FREQ = 4 -- only update the quality renders once every x frames
FISH_COLISSION_CHECK_FREQ = 2 -- how often to update the colission
FISH_EASE = ease.inOutSine -- ease used for fish movement

FISH_ANGLE = 30 -- bigger angle allows for fish to swim further down/up at once
FISH_FOLLOW_RANDOM = 10

FISH_FOOD_CHECK_FREQ = 1 -- how often to check for food
FISH_FOOD_1_COOLDOWN = 10
FISH_FOOD_2_COOLDOWN = 20 -- needs to be checked a second time, not sure if this is correct
FISH_FOOD_3_COOLDOWN = 25
FISH_FOOD_HUNGRY = -5
FISH_FOOD_DEAD = -13

FISH_AGE_MEDIUM = 25 -- the age, in seconds, needed for a guppy to become a medium guppy
FISH_AGE_BIG = 65 -- see above, but for big
FISH_AGE_KING = 15 * 60 -- see above, but for king (15 minutes)

FISH_SIZE = 6 -- how many large guppies can you fit on the screen

HEADER_HEIGHT = 68

FOOD_HITBOX = 0.08

DEBUG_FISH_PATH_SUBDIVISIONS = 50
DEBUG_FISH_PREDICT_AMOUNT = 0.5