return function(money, dt, sheets)
  for i,f in ipairs(money) do
    if not f.collected then
      f.y = f.y + dt * f.speed
      f.y = clamp(f.y, 0, 0.9)
    end
    f.time = f.time + dt

    if f.y == 0.9 and not f.collected then
      f.deathtimer = f.deathtimer + dt
    end
    if f.collected then
      f.collecttimer = f.collecttimer + dt
    end

    if f.deathtimer > 1 or f.collecttimer > 1 then
      table.remove(money, i)
    end

    local dist = math.abs(love.mouse.getX() - f.x * love.graphics.getWidth()) + math.abs(love.mouse.getY() - f.y * love.graphics.getHeight())
    if dist < sheets.coin1.width/2 and not f.collected then
      setCursor('hover')
    end
  end
end