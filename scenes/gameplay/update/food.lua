return function(food, dt)
  for i,f in ipairs(food) do
    f.y = f.y + dt * f.speed
    f.time = f.time + dt
    f.y = clamp(f.y, 0, 0.9)

    if f.y == 0.9 then
      f.deathtimer = f.deathtimer + dt
    end

    if f.deathtimer > 1 then
      table.remove(food, i)
    end
  end
end