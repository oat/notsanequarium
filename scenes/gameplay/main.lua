local self = {}

local sheets = {}

local food = {}
local feesh = {}
local money = {}

local balance = 100

local foodtier = 1
local foodcount = 1

local starpotionequipped = false
local moneyflashtimer = 0

local headerbuttons = {
  {
    cost = 100,
    sprite = 'guppy',
    openanim = 1,
    tooltip = 'buy guppy',
    open = false,
    closed = false,
    func = function()
      playSound('splash', 0.7, 0.8)
      table.insert(feesh, constr.fish(math.random(), 0.3))
    end
  },
  {
    cost = 200,
    sprite = 'food',
    tier = foodtier,
    openanim = 1,
    tooltip = 'upgrade food quality',
    open = false,
    closed = false,
    func = function(self)
      self.openanim = 0
      foodtier = foodtier + 1
      if foodtier >= 3 then
        self.open = false
        self.closed = true
      else
        self.tier = foodtier
      end
    end
  },
  {
    cost = 300,
    sprite = 'foodcount',
    tier = foodcount,
    openanim = 1,
    tooltip = 'upgrade food quantity',
    open = false,
    closed = false,
    func = function(self)
      self.openanim = 0
      foodcount = foodcount + 1
      if foodcount >= 9 then
        self.open = false
        self.closed = true
      else
        self.tier = foodcount
      end
    end
  },
  {
    cost = 1000,
    sprite = 'carnivore',
    tooltip = 'buy carnivore',
    openanim = 1,
    open = false,
    closed = false,
    func = function()
      playSound('splash', 0.7, 0.8)
      table.insert(feesh, constr.fish(math.random(), 0.3, 4))
    end
  },
  {
    cost = 250,
    sprite = 'starpotion',
    tooltip = 'buy star potion',
    openanim = 1,
    open = false,
    closed = false,
    func = function()
      starpotionequipped = true
    end
  }
}

local sheets = {}
paused = false

local function fishsprite(size, hungry, anim)
  -- anim is turn, swim, eat or die
  if anim == 'die' then hungry = false end

  local spritename = size .. '_' .. (hungry and 'hungry_' or '') .. anim
  local spr = sprites['fish/' .. spritename]
  return newAnimation(spr, spr:getWidth()/10, spr:getHeight())
end

function self.load()
  sheets.wavecenter = newAnimation(sprites['wave/wavecenter'], sprites['wave/wavecenter']:getWidth(), sprites['wave/wavecenter']:getHeight()/12)
  sheets.waveside = newAnimation(sprites['wave/waveside'], sprites['wave/waveside']:getWidth(), sprites['wave/waveside']:getHeight()/12)

  sheets.food1 = newAnimation(sprites['food/1'], sprites['food/1']:getWidth()/10, sprites['food/1']:getHeight())
  sheets.food2 = newAnimation(sprites['food/2'], sprites['food/2']:getWidth()/10, sprites['food/2']:getHeight())
  sheets.food3 = newAnimation(sprites['food/3'], sprites['food/3']:getWidth()/10, sprites['food/3']:getHeight())
  sheets.potion = newAnimation(sprites['food/potion'], sprites['food/potion']:getWidth()/10, sprites['food/potion']:getHeight())

  sheets.buttonopen = newAnimation(sprites['header/button_open'], sprites['header/button_open']:getWidth()/3, sprites['header/button_open']:getHeight())

  sheets.coin1 = newAnimation(sprites['money/coin1'], sprites['money/coin1']:getWidth()/10, sprites['money/coin1']:getHeight())
  sheets.coin2 = newAnimation(sprites['money/coin2'], sprites['money/coin2']:getWidth()/10, sprites['money/coin2']:getHeight())
  sheets.star = newAnimation(sprites['money/star'], sprites['money/star']:getWidth()/10, sprites['money/star']:getHeight())
  sheets.diamond = newAnimation(sprites['money/diamond'], sprites['money/diamond']:getWidth()/10, sprites['money/diamond']:getHeight())
  sheets.chest = newAnimation(sprites['money/chest'], sprites['money/chest']:getWidth()/10, sprites['money/chest']:getHeight())

  for i = 1, 2 do
    table.insert(feesh, constr.fish(math.random(), math.random(), 0))
  end
end

function self.update(dt)
  if paused then dt = 0 end

  bench.startBenchmark('update_buttons')

  moneyflashtimer = moneyflashtimer - dt

  local x = 19
  local y = 3
  for b,btn in ipairs(headerbuttons) do
    btn.openanim = btn.openanim + dt * 6
    local size = (love.graphics.getWidth()/640)
    local hovered = mouseOverBox(x * size, y * size, sprites['header/buttonbg']:getWidth() * size, sprites['header/buttonbg']:getHeight() * size)

    if btn.open and hovered and not paused then
      setCursor('hover')
    end

    local incr = 69 -- its like button positions but forcefully shoved into a recursive function :D
    if b == 2 then incr = 57 end
    if b >= 3 then incr = 73 end
    x = x + incr
  end

  local s = sprites['header/optionsbutton_hover']
  local size = love.graphics.getWidth() / 640
  local hovered = mouseOverBox(love.graphics.getWidth() - 115 * size, size * (HEADER_HEIGHT - 65), s:getWidth() * size, s:getHeight() * size)
  if hovered and not paused then
    setCursor('hover')
  end

  bench.stopBenchmark('update_buttons')

  bench.startBenchmark('update_food')
  require('scenes.gameplay.update.food')(food, dt)
  bench.stopBenchmark('update_food')

  bench.startBenchmark('update_money')
  require('scenes.gameplay.update.money')(money, dt, sheets)
  bench.stopBenchmark('update_money')

  bench.startBenchmark('update_fish')
  require('scenes.gameplay.update.fish')(feesh, dt, food, headerbuttons, money)
  bench.stopBenchmark('update_fish')
  
  if debug then
    for _,b in ipairs(headerbuttons) do
      b.open = true
    end
  end
end

function self.draw()
  bench.startBenchmark('render_tank')

  local sw, sh = love.graphics.getDimensions()
  local headerheight = HEADER_HEIGHT * sw/640
  local yscale = (sh-headerheight)/sh

  local sample = fishsprite('medium', false, 'swim')
  local spritescale = (math.min(sw, sh)/FISH_SIZE) / math.min(sample.width, sample.height)

  stretchto(sprites['bg/1'], 0, headerheight - HEADER_HEIGHT, 0, sw, sh - (headerheight - HEADER_HEIGHT))

  -- waves
  bench.startBenchmark('render_wave')
  require('scenes.gameplay.draw.wave')(headerheight, sheets)
  bench.stopBenchmark('render_wave')

  -- shadow
  bench.startBenchmark('render_shadow')
  require('scenes.gameplay.draw.shadow')(feesh, spritescale)
  bench.stopBenchmark('render_shadow')

  bench.startBenchmark('render_food')
  require('scenes.gameplay.draw.food')(food, sheets, spritescale, starpotionequipped)
  bench.stopBenchmark('render_food')

  -- all the fish
  bench.startBenchmark('render_fish')
  require('scenes.gameplay.draw.fish')(feesh, spritescale, fishsprite)
  bench.stopBenchmark('render_fish')
  bench.stopBenchmark('render_tank')
  
  bench.startBenchmark('render_header')
  require('scenes.gameplay.draw.header')(headerheight, fishsprite, headerbuttons, sheets, balance, moneyflashtimer)
  bench.stopBenchmark('render_header')

  bench.startBenchmark('render_money')
  require('scenes.gameplay.draw.money')(money, sheets, spritescale)
  bench.stopBenchmark('render_money')
end

function self.mousepressed(x, y, b)
  if b == 1 and not paused then
    local s = sprites['header/optionsbutton_hover']
    local size = love.graphics.getWidth() / 640
    local hovered = mouseOverBox(love.graphics.getWidth() - 115 * size, size * (HEADER_HEIGHT - 65), s:getWidth() * size, s:getHeight() * size)
    if hovered then
      paused = true
    end

    for _,m in ipairs(money) do
      local dist = math.abs(x - m.x * love.graphics.getWidth()) + math.abs(y - m.y * love.graphics.getHeight())
      if dist < sheets.coin1.width/2 and not m.collected then
        m.collected = true
        m.deathtimer = 0
        playSound('collect', 1, 1 + math.random() * 0.2 - 0.1)

        if m.type == 1 then balance = balance + 15 end
        if m.type == 2 then balance = balance + 35 end
        if m.type == 3 then balance = balance + 40 end
        if m.type == 4 then balance = balance + 200 end
        if m.type == 5 then balance = balance + 2000 end

        return
      end
    end
  end

  if b == 1 and y > HEADER_HEIGHT and not paused then
    if starpotionequipped then
      table.insert(food, constr.food(x/love.graphics.getWidth(), y/love.graphics.getHeight(), 4))
      playSound('dropfood')
      starpotionequipped = false
    elseif #food < foodcount then
      if balance >= 5 or debug then
        table.insert(food, constr.food(x/love.graphics.getWidth(), y/love.graphics.getHeight(), foodtier))
        playSound('dropfood')
        balance = balance - 5
      else
        playSound('buzzer')
        moneyflashtimer = 1.2
      end
    end

    return
  end

  local headerheight = HEADER_HEIGHT * love.graphics.getWidth()/640
  local size = headerheight / HEADER_HEIGHT

  if b == 1 and not paused then
    local x = 19
    for i = 1, 7 do
      local hovered = mouseOverBox(x * size, 3 * size, sprites['header/buttonbg']:getWidth() * size, sprites['header/buttonbg']:getHeight() * size)
      
      if hovered then
        if headerbuttons[i] and headerbuttons[i].open then
          if balance >= headerbuttons[i].cost or debug then
            headerbuttons[i].func(headerbuttons[i])
            playSound('buttonclick')
            balance = balance - headerbuttons[i].cost
          else
            playSound('buzzer')
            moneyflashtimer = 1.2
          end
        end
      end

      local incr = 69 -- its like button positions but forcefully shoved into a recursive function :D
      if b == 2 then incr = 57 end
      if b >= 3 then incr = 73 end
      x = x + incr
    end
  end
end

return self