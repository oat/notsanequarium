return function(feesh, spritescale, fishsprite)
  local sw, sh = love.graphics.getDimensions()
  for i, n in ipairs(feesh) do
    local x = n.render.x * sw
    local y = n.render.y * sh

    -- rest of feesh
    local size = 'small'
    local anim = 'swim'

    local sample = fishsprite('medium', false, 'swim')

    local sizex = 1
    local turn = n.render.turn
    if n.render.turndir == -1 then turn = 1 - turn; sizex = -1 end

    local turnframe = math.floor(turn * (#sample.quads - 1)) + 1
    if #sample.quads == turnframe then
      sizex = -1 * sizex
      turnframe = 1
    end
    
    local frame = math.floor(n.render.swim * (#sample.quads - 1)) + 1

    if turnframe ~= 1 and turnframe ~= #sample.quads then
      anim = 'turn'
      frame = turnframe
    end

    if n.render.eattimer <= 1 then
      anim = 'eat'
      frame = math.floor(n.render.eattimer * (#sample.quads - 1)) + 1
    end

    if n.dead then
      anim = 'die'
      local a = math.min(math.abs(n.eattimer - FISH_FOOD_DEAD) * 1.4, 1) - n.render.deathanim * 0.4
      frame = math.floor(a * (#sample.quads - 1)) + 1
    end

    local angle = n.render.angle
    if angle > math.pi/2 then
      angle = angle - math.pi
    end
    if angle < -math.pi/2 then
      angle = angle + math.pi
    end

    angle = angle * math.max(math.min((-math.abs(angle) + math.pi * 0.5) / (math.pi * 0.5) * 2, 1), 0)
    angle = angle * 0.5

    if n.size == 0 then
      size = 'small'
    elseif n.size == 1 then
      size = 'medium'
    elseif n.size == 2 then
      size = 'big'
    elseif n.size == 3 then
      size = 'king'
    elseif n.size == 4 then
      size = 'carnivore'
    end

    local sheet = fishsprite(size, false, anim)
    local sadsheet = fishsprite(size, true, anim)

    local alpha = n.render.hungry == 1 and 0 or 1
    local allalpha = 1
    if n.star then allalpha = 0.8 end
    love.graphics.setColor(1, 1, 1, (alpha - n.render.deathanim) * allalpha)
    love.graphics.draw(sheet.spriteSheet, sheet.quads[math.max(math.min(frame, #sample.quads), 1)], x, y, angle, sizex * spritescale, spritescale, sample.width/2, sample.height/2)
    love.graphics.setColor(1, 1, 1, (n.render.hungry - n.render.deathanim) * allalpha)
    love.graphics.draw(sadsheet.spriteSheet, sheet.quads[math.max(math.min(frame, #sample.quads), 1)], x, y, angle, sizex * spritescale, spritescale, sample.width/2, sample.height/2)

    if n.star then
      local mult = (math.sin(love.timer.getTime() / 4) / 2 + 0.5) * 0.1 + 0.9

      love.graphics.setBlendMode('add')
      love.graphics.setColor(1, 1, 1, (alpha - n.render.deathanim) * mult)
      love.graphics.draw(sheet.spriteSheet, sheet.quads[math.max(math.min(frame, #sample.quads), 1)], x, y, angle, sizex * spritescale, spritescale, sample.width/2, sample.height/2)
      love.graphics.setColor(1, 1, 1, (n.render.hungry - n.render.deathanim) * mult)
      love.graphics.draw(sadsheet.spriteSheet, sheet.quads[math.max(math.min(frame, #sample.quads), 1)], x, y, angle, sizex * spritescale, spritescale, sample.width/2, sample.height/2)
      love.graphics.setBlendMode('alpha')
    end

    love.graphics.setColor(1, 1, 1)
    if debug then love.graphics.print(shrt(n.eattimer), x + 20, y + 20) end

    if debug then
      for _,e in ipairs(n.eases) do
        love.graphics.setColor(1, 0, 0, 0.75)
        love.graphics.line(e.fromx * sw, e.fromy * sh, e.x * sw, e.y * sh)

        love.graphics.setColor(0, 0, 1, 0.75)
        love.graphics.line(mix(e.fromx, e.x, FISH_EASE(e.a)) * sw, mix(e.fromy, e.y, FISH_EASE(e.a)) * sh, n.render.x * sw, n.render.y * sh)
      end

      local subdiv = DEBUG_FISH_PATH_SUBDIVISIONS
      local adv = DEBUG_FISH_PREDICT_AMOUNT
      local pos = {}
      local valid = {}

      for i = 1, subdiv do
        local a = ((i - 1) / (subdiv - 1)) * (adv * 2) - adv

        local sumx = 0
        local sumy = 0
        local mina = 0
        local maxa = 1
        for _, e in ipairs(n.eases) do
          local a = e.a + a
          mina = math.min(mina, a)
          maxa = math.max(maxa, a)
          sumx = sumx + mix(e.fromx, e.x, FISH_EASE(a))
          sumy = sumy + mix(e.fromy, e.y, FISH_EASE(a))
        end
    
        table.insert(pos, sumx / #n.eases * sw)
        table.insert(pos, sumy / #n.eases * sh)
        table.insert(valid, not (maxa > 1 or mina < 0))
      end

      for i = 0, #pos/2 - 1 do
        local x1 = pos[i * 2 + 1]
        local y1 = pos[i * 2 + 1 + 1]
        local x2 = pos[i * 2 + 2 + 1]
        local y2 = pos[i * 2 + 3 + 1]
        local valid = valid[i + 1]

        if not x2 or not y2 then break end

        love.graphics.setColor(0, 1, 0, 1)
        if not valid then love.graphics.setColor(0, 0.5, 1, 0.7) end

        love.graphics.line(x1, y1, x2, y2)
      end
    end
  end
end