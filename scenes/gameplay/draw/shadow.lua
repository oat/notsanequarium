return function(feesh, spritescale)
  local sw, sh = love.graphics.getDimensions()
  if options.render_shadow then
    for i, n in ipairs(feesh) do
      love.graphics.setColor(1, 1, 1, n.render.y + 0.2 - n.render.deathanim)
      local sizes = {0.55, 0.7, 1}
      local size = sizes[n.size + 1] or 1
      love.graphics.draw(sprites['shadow'], n.render.x * sw - (sprites['shadow']:getWidth() * spritescale * size)/2, sh - sh * 0.18 - (sprites['shadow']:getHeight() * spritescale * size)/2 + n.render.y * sh * 0.08, 0, spritescale * size, spritescale * size)
    end
  end
end