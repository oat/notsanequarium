return function(headerheight, fishsprite, headerbuttons, sheets, balance, moneyflashtimer)
  local sw, sh = love.graphics.getDimensions()
  local base = sprites['header/base']
  local size = headerheight / HEADER_HEIGHT
  love.graphics.setColor(1, 1, 1, 1)
  love.graphics.draw(base, 0, 0, 0, size, size)

  local tooltip
  local tooltipText
  local tooltipx
  local tooltipy

  -- the game is making me do this. im sorry
  local x = 19
  local y = 3
  for b = 1, 7 do
    local hovered = mouseOverBox(x * size, y * size, sprites['header/buttonbg']:getWidth() * size, sprites['header/buttonbg']:getHeight() * size) and not paused
    local btn = headerbuttons[b]

    if (btn and not btn.open) or not btn then
      -- draw nothing
    elseif hovered and love.mouse.isDown(1) then
      love.graphics.draw(sprites['header/buttonbg_down'], x * size, y * size, 0, size, size)
      if not tooltip then tooltip = btn.tooltip end
    elseif hovered then
      love.graphics.draw(sprites['header/buttonbg_hover'], x * size, y * size, 0, size, size)
      if not tooltip then tooltip = btn.tooltip end
    else
      love.graphics.draw(sprites['header/buttonbg'], x * size, y * size, 0, size, size)
    end

    if tooltip and btn and not tooltipText then
      if not btn.tooltipText then
        btn.tooltipText = love.graphics.newText(fonts.pix, tooltip)
      end

      tooltipText = btn.tooltipText
      tooltipx = x * size + sprites['header/buttonbg']:getWidth() * size * 0.5 - tooltipText:getWidth()/2
      tooltipy = y * size + sprites['header/buttonbg']:getWidth() * size
    end

    if btn then
      if btn.open then
        -- sprite inside
        if btn.sprite == 'guppy' or btn.sprite == 'carnivore' then
          local sheet = fishsprite(btn.sprite == 'carnivore' and 'carnivore' or 'medium', false, 'swim')
          local frame = math.floor((love.timer.getTime() * 2) % 1 * #sheet.quads) + 1
          local scale = (sprites['header/buttonbg']:getWidth() / sheet.width) * 0.9
          if btn.sprite == 'carnivore' then scale = scale * 0.7 end
          local offset = (sprites['header/buttonbg']:getWidth() * size) / 2
          love.graphics.draw(sheet.spriteSheet, sheet.quads[frame], x * size + offset, y * size + offset*0.75, 0, size * scale, size * scale, sheet.width/2, sheet.width/2)
        elseif btn.sprite == 'food' then
          local sheets = {sheets.food2, sheets.food3}
          local sheet = sheets[btn.tier]

          local scale = (sprites['header/buttonbg']:getWidth() / sheet.width) * 0.65
          local offset = (sprites['header/buttonbg']:getWidth() * size) / 2
          love.graphics.draw(sheet.spriteSheet, sheet.quads[1], x * size + offset, y * size + offset*0.75, 0, size * scale, size * scale, sheet.width/2, sheet.width/2)
        elseif btn.sprite == 'foodcount' then
          love.graphics.setFont(fonts.continuum)
          local offset = (sprites['header/buttonbg']:getWidth() * size) / 2

          local bordersize = 1
          for _,p in ipairs({{0, 1}, {1, 0}, {1, 1}, {-1, 0}, {0, -1}, {-1, -1}, {1, -1}, {-1, 1}}) do
            love.graphics.setColor(0, 0, 0, 0.5)
            love.graphics.printf(btn.tier + 1, round(x * size) + p[1] * bordersize, round(y * size + offset*0.75 - fonts.continuum:getHeight()/2) + p[2] * bordersize, round(sprites['header/buttonbg']:getWidth() * size), 'center')
          end

          love.graphics.setColor(0, 1, 0)
          love.graphics.printf(btn.tier + 1, round(x * size), round(y * size + offset*0.75 - fonts.continuum:getHeight()/2), round(sprites['header/buttonbg']:getWidth() * size), 'center')
        elseif btn.sprite == 'starpotion' then
          local sheet = sheets.potion

          local scale = (sprites['header/buttonbg']:getWidth() / sheet.width) * 0.65
          local offset = (sprites['header/buttonbg']:getWidth() * size) / 2
          love.graphics.draw(sheet.spriteSheet, sheet.quads[1], x * size + offset, y * size + offset*0.75, 0, size * scale, size * scale, sheet.width/2, sheet.width/2)
        end

        -- price
        love.graphics.setFont(fonts.pix)
        local font = love.graphics.getFont()
        love.graphics.setColor(0, 1, 0)
        love.graphics.printf('$' .. btn.cost, round(x * size), round(y * size + 51 * size - font:getHeight()/2), round(sprites['header/buttonbg']:getWidth() * size), 'center')
        love.graphics.setColor(1, 1, 1)
        love.graphics.setFont(fonts.default)

        -- reflection
        love.graphics.setBlendMode('add')
        love.graphics.draw(sprites['header/reflection'], x * size, y * size, 0, size, size)
        love.graphics.setBlendMode('alpha')
      end

      -- open/close anim
      if (btn and btn.openanim < 1) then
        local sheet = sheets.buttonopen
        local anim = 0
        if btn then anim = btn.openanim end
        local frame = math.floor(anim % 1 * #sheet.quads) + 1
        love.graphics.draw(sheet.spriteSheet, sheet.quads[frame], x * size, y * size, 0, size, size)
      end
    end

    local incr = 69 -- its like button positions but forcefully shoved into a recursive function :D
    if b == 2 then incr = 57 end
    if b >= 3 then incr = 73 end
    x = x + incr
  end

  -- options text
  local s = sprites['header/optionsbutton_hover']
  local hovered = mouseOverBox(sw - 115 * size, size * (HEADER_HEIGHT - 65), s:getWidth() * size, s:getHeight() * size) and not paused

  if hovered and love.mouse.isDown(1) then 
    love.graphics.draw(sprites['header/optionsbutton_down'], sw - 115 * size, size * (HEADER_HEIGHT - 65), 0, size, size)
  elseif hovered then
    love.graphics.draw(sprites['header/optionsbutton_hover'], sw - 115 * size, size * (HEADER_HEIGHT - 65), 0, size, size)
  end

  -- money count
  if love.timer.getTime() % 0.25 < 0.125 and moneyflashtimer > 0 then love.graphics.draw(sprites['header/moneyflash'], sw * 0.851, (HEADER_HEIGHT - 29) * size, 0, size, size) end
  love.graphics.setFont(fonts.continuum)
  love.graphics.setColor(179/255, 254/255, 89/255)
  local leftpad = 100
  love.graphics.printf(balance, round(sw * 0.965 - leftpad), round((HEADER_HEIGHT - 25) * size), leftpad, 'right')

  -- tooltips
  if tooltipText and not paused then
    love.graphics.setColor(254/255, 254/255, 199/255)
    love.graphics.rectangle('fill', round(tooltipx - 5), round(tooltipy - 5), round(tooltipText:getWidth() + 10), round(tooltipText:getHeight() + 10))
    love.graphics.setColor(2/255, 2/255, 2/255)
    love.graphics.rectangle('line', round(tooltipx - 5), round(tooltipy - 5), round(tooltipText:getWidth() + 10), round(tooltipText:getHeight() + 10))

    love.graphics.setFont(fonts.pix)
    love.graphics.draw(tooltipText, round(tooltipx), round(tooltipy))
  end

  love.graphics.setFont(fonts.default)
end