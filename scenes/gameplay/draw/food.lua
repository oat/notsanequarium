local sheetNames = {
  'food1', 'food2', 'food3', 'potion'
}

return function(food, sheets, spritescale, starpotionequipped)
  local sw, sh = love.graphics.getDimensions()
  for _,f in ipairs(food) do
    local sheet = sheets[sheetNames[f.type]]
    local x = f.x * sw
    local y = f.y * sh
    local frame = math.floor((f.time%1) * #sheet.quads) + 1

    love.graphics.setColor(1, 1, 1, 1 - f.deathtimer)

    love.graphics.draw(sheet.spriteSheet, sheet.quads[math.max(math.min(frame, #sheet.quads), 1)], x, y, 0, spritescale, spritescale, sheet.width/2, sheet.height/2)
  end

  if starpotionequipped then
    local sheet = sheets.potion
    love.graphics.setColor(1, 1, 1, 0.8)
    love.graphics.draw(sheet.spriteSheet, sheet.quads[1], love.mouse.getX(), love.mouse.getY(), 0, spritescale, spritescale, sheet.width/2, sheet.height/2)
  end
end