local moneysheets = {
  'coin1', 'coin2', 'star', 'diamond', 'chest'
}

return function(money, sheets, spritescale)
  local sw, sh = love.graphics.getDimensions()
  for _,f in ipairs(money) do
    local sheet = sheets[moneysheets[f.type]]
    local x = mix(f.x * sw, sw / 9 * 8, ease.outCubic(f.collecttimer))
    local y = mix(f.y * sh, HEADER_HEIGHT - 20, ease.outCubic(f.collecttimer))
    local frame = math.floor((f.time%1) * #sheet.quads) + 1

    love.graphics.setColor(1, 1, 1, 1 - f.deathtimer)
    love.graphics.draw(sheet.spriteSheet, sheet.quads[math.max(math.min(frame, #sheet.quads), 1)], x, y, 0, spritescale, spritescale, sheet.width/2, sheet.height/2)
  end
end