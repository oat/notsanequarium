return function(headerheight, sheets)
  local sw, sh = love.graphics.getDimensions()
  local wavecount = round(sw / sprites['wave/wavecenter']:getWidth())
  local wavescale = sw / (wavecount * sprites['wave/wavecenter']:getWidth())
  for i = 1, wavecount do
    local a = (i - 1) / wavecount
    local x = a * sw
    local frame = round((1 - math.abs(love.timer.getTime()%2-1)) * #sheets.wavecenter.quads)
    love.graphics.setBlendMode('add')

    local sheet = sheets.wavecenter
    local sizex = 1
    if i == 1 or i == wavecount then
      sheet = sheets.waveside
    end
    if i == wavecount then
      sizex = -1
    end
    love.graphics.draw(sheet.spriteSheet, sheet.quads[math.max(frame, 1)], x + (sprites['wave/wavecenter']:getWidth() * wavescale)/2, headerheight + 20, 0, wavescale * sizex, wavescale, sprites['wave/wavecenter']:getWidth()/2)
  end
  love.graphics.setBlendMode('alpha')
end