require 'const'
require 'util'

constr = require 'constructors'

require 'lib.audio'
assets = require 'lib.assets'
ease = require 'lib.ease'
bench = require 'lib.benchmark'
tick = require 'lib.tick'
lurker = require 'lib.lurker'

scenes = {
  gameplay = require('scenes/gameplay/main')
}
local defaultscene = scenes.gameplay

scene = defaultscene

sprites = {}
sound_path = {}
music_path = {}
fonts = {}
cursors = {}

debug = false
is_mobile = love.system.getOS() == "Android" or love.system.getOS() == "iOS"

options = { -- temporary
  pause_on_unfocus = true,
  render_shadow = true,
}

function newAnimation(image, width, height)
  local animation = {}
  animation.spriteSheet = image;
  animation.quads = {};
  animation.width = width
  animation.height = height

  for y = 0, image:getHeight() - height, height do
    for x = 0, image:getWidth() - width, width do
      table.insert(animation.quads, love.graphics.newQuad(x, y, width, height, image:getDimensions()))
    end
  end

  return animation
end

function love.load()
  assets.clear()
  assets.load('assets')

  fonts.pix = love.graphics.newFont('assets/fonts/pix.ttf', 6)
  fonts.continuum = love.graphics.newFont('assets/fonts/cont.ttf', 14)
  fonts.default = love.graphics.newFont(12)

  if scene.load then scene.load() end

  cursors.default = love.mouse.newCursor('assets/sprites/cursor/pointer.png', sprites['cursor/pointer']:getWidth()/2, sprites['cursor/pointer']:getHeight()/2)
  cursors.hover = love.mouse.newCursor('assets/sprites/cursor/hand.png', sprites['cursor/hand']:getWidth()/2, sprites['cursor/hand']:getHeight()/2)
  cursors.drag = love.mouse.newCursor('assets/sprites/cursor/dragging.png', sprites['cursor/dragging']:getWidth()/2, sprites['cursor/dragging']:getHeight()/2)

  if not is_mobile then
    love.mouse.setCursor(cursors.default)
  end

  if is_mobile then
    local winwidth, winheight = love.graphics.getDimensions()
    love.window.setMode(winwidth, winheight, {borderless=true, resizable=false, minwidth=705, minheight=510, fullscreen=true})
  end
end

local cursor
local lastcursor = nil
function setCursor(mouse)
  cursor = mouse
end

frame = 0
function love.update(dt)
  cursor = nil
  frame = frame + 1
  bench.update()
  tick.update(dt)
  lurker.update()

  bench.startBenchmark('update')
  if scene.update then scene.update(dt) end
  bench.stopBenchmark('update')

  if not cursor then cursor = 'default' end
  if (not is_mobile) and (cursor ~= lastcursor) then
    love.mouse.setCursor(cursors[cursor])
  end
  lastcursor = cursor
end

function love.draw()
  love.graphics.setFont(fonts.default)
  love.graphics.setColor(1, 1, 1)
  local sw, sh = love.graphics.getDimensions()
  bench.startBenchmark('render')

  if scene.draw then scene.draw() end

  love.graphics.setColor(1, 1, 1, 1)
  love.graphics.print('FPS: ' .. 1 / love.timer.getDelta(), 0, sh - 16)

  if paused then
    love.graphics.setBlendMode('subtract')
    love.graphics.setColor(1, 1, 1, 0.1)
    love.graphics.rectangle('fill', 0, 0, sw, sh)
    love.graphics.setBlendMode('alpha')
    love.graphics.setColor(0, 0, 0, 0.6)
    love.graphics.rectangle('fill', 0, 0, sw, sh)
    love.graphics.setColor(1, 1, 1)
    love.graphics.print('paused, space to unpause', 20, 20)

    y = 30
    for k,v in pairs(options) do
      y = y + 16

      local text = love.graphics.newText(love.graphics.getFont(), k .. ' = ' .. (v and 'true' or 'false')) -- todo: cache!!!!!!!! please
      local invertcol = mouseOverBox(20, y + 1, text:getWidth(), text:getHeight())

      if invertcol then love.graphics.setColor(1, 1, 1) else love.graphics.setColor(0, 0, 0) end
      love.graphics.rectangle('fill', 20, y + 1, text:getWidth(), text:getHeight())
      if invertcol then love.graphics.setColor(0, 0, 0) else love.graphics.setColor(1, 1, 1) end
      love.graphics.draw(text, 20, y)
    end
  end

  if debug then bench.renderBenchmark() end
  bench.stopBenchmark('render')
end

function love.mousepressed(x, y, b)
  if scene.mousepressed then scene.mousepressed(x, y, b) end

  if is_mobile then
    if paused then paused = not paused end
  end

  y = 30
  for k,v in pairs(options) do
    y = y + 16
    
    local text = love.graphics.newText(love.graphics.getFont(), k .. ' = ' .. (v and 'true' or 'false')) -- todo: cache!!!!!!!! please
    if mouseOverBox(20, y + 1, text:getWidth(), text:getHeight()) then
      options[k] = not v
    end
  end
end

function love.mousereleased(x, y, b)
  if scene.mousereleased then scene.mousereleased(x, y, b) end
end

function love.keypressed(key)
  if key == 'f3' then
    debug = not debug
  elseif key == 'space' then
    paused = false
  end
end

function love.focus(f)
  if not f and options.pause_on_unfocus then
    paused = true
  end
end
