require 'lib.audio'

local assets = {}
local rawsprites = {}

function assets.clear()
  print("ℹ️ clearing assets")

  sprites = {}
  rawsprites = {}
  sound_path = {}
  music_path = {}
end

function assets.load(base)
  print("ℹ️ loading " .. base)

  setmetatable(sprites, {__index = function(tbl, i)
    if rawsprites[i] then return rawsprites[i] end
    local sprite = love.graphics.newImage('assets/sprites/' .. i .. '.png')
    rawsprites[i] = sprite
    return sprite
  end})

  --assets.addSprites(base)
  --print("✓ added sprites")

  assets.addAudio(base)
  print("✓ added audio")
end

function assets.addSprites(base, d)
  local dir = base.."/sprites"
  if d then
    dir = dir .. "/" .. d
  end
  local files = love.filesystem.getDirectoryItems(dir)
  for _,file in ipairs(files) do
    if string.sub(file, -4) == ".png" then
      local spritename = string.sub(file, 1, -5)
      local sprite = love.graphics.newImage(dir .. "/" .. file)
      if d then
        spritename = d .. "/" .. spritename
      end
      sprites[spritename] = sprite
      --print(colr.cyan("ℹ️ added sprite "..spritename))
    elseif love.filesystem.getInfo(dir .. "/" .. file).type == "directory" then
      --print(colr.cyan("ℹ️ found sprite dir: " .. file))
      local newdir = file
      if d then
        newdir = d .. "/" .. newdir
      end
      assets.addSprites(base, newdir)
    end
  end
end

function assets.addAudio(base, d, type)
  local dir = base.."/audio"
  if d then
    dir = dir .. "/" .. d
  end
  local files = love.filesystem.getDirectoryItems(dir)
  for _,file in ipairs(files) do
    if love.filesystem.getInfo(dir .. "/" .. file).type == "directory" then
      local newdir = file
      if d then
        newdir = d .. "/" .. newdir
      end
      assets.addAudio(base, newdir, type or file)
    else
      local audioname = file
      if file:ends(".wav") then audioname = file:sub(1, -5) end
      if file:ends(".mp3") then audioname = file:sub(1, -5) end
      if file:ends(".ogg") then audioname = file:sub(1, -5) end
      if file:ends(".flac") then audioname = file:sub(1, -5) end
      if file:ends(".xm") then audioname = file:sub(1, -4) end
      --[[if d then
        audioname = d .. "/" .. audioname
      end]]
      if type == "sfx" then
        sound_path[audioname] = dir .. "/" .. file

        if sounds then
          registerSound(audioname, (sounds[audioname] or {}).volume or 1)
        end
      elseif type == "bgm" then
        music_path[audioname] = dir .. "/" .. file
      end
      --print("ℹ️ audio "..audioname.." added")
    end
  end
end

return assets