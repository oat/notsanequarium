local this = {}

local sqrt = math.sqrt
local sin = math.sin
local cos = math.cos
local pow = math.pow
local exp = math.exp
local pi = math.pi
local abs = math.abs

function this.linear(t) return t end
function this.inQuad(t) return t * t end
function this.outQuad(t) return -t * (t - 2) end
function this.inOutQuad(t)
  t = t * 2
  if t < 1 then
    return 0.5 * t ^ 2
  else
    return 1 - 0.5 * (2 - t) ^ 2
  end
end
function this.inCubic(t) return t * t * t end
function this.outCubic(t) return 1 - (1 - t) ^ 3 end
function this.inOutCubic(t)
  t = t * 2
  if t < 1 then
    return 0.5 * t ^ 3
  else
    return 1 - 0.5 * (2 - t) ^ 3
  end
end
function this.inQuart(t) return t * t * t * t end
function this.outQuart(t) return 1 - (1 - t) ^ 4 end
function this.inOutQuart(t)
  t = t * 2
  if t < 1 then
    return 0.5 * t ^ 4
  else
    return 1 - 0.5 * (2 - t) ^ 4
  end
end
function this.inQuint(t) return t ^ 5 end
function this.outQuint(t) return 1 - (1 - t) ^ 5 end
function this.inOutQuint(t)
  t = t * 2
  if t < 1 then
    return 0.5 * t ^ 5
  else
    return 1 - 0.5 * (2 - t) ^ 5
  end
end
function this.inExpo(t) return 1000 ^ (t - 1) - 0.001 end
function this.outExpo(t) return 0.999 - 1000 ^ -t end
function this.inOutExpo(t)
  t = t * 2
  if t < 1 then
    return 0.5 * 1000 ^ (t - 1) - 0.0005
  else
    return 0.9995 - 0.5 * 1000 ^ (1 - t)
  end
end
function this.inCirc(t) return 1 - sqrt(1 - t * t) end
function this.outCirc(t) return sqrt(-t * t + 2 * t) end
function this.inOutCirc(t)
  t = t * 2
  if t < 1 then
    return 0.5 - 0.5 * sqrt(1 - t * t)
  else
    t = t - 2
    return 0.5 + 0.5 * sqrt(1 - t * t)
  end
end

function this.inElastic(t)
  t = t - 1
  return -(pow(2, 10 * t) * sin((t - 0.075) * (2 * pi) / 0.3))
end
function this.outElastic(t)
  return pow(2, -10 * t) * sin((t - 0.075) * (2 * pi) / 0.3) + 1
end
function this.inOutElastic(t)
  t = t * 2 - 1
  if t < 0 then
    return -0.5 * pow(2, 10 * t) * sin((t - 0.1125) * 2 * pi / 0.45)
  else
    return pow(2, -10 * t) * sin((t - 0.1125) * 2 * pi / 0.45) * 0.5 + 1
  end
end

function this.inBack(t) return t * t * (2.70158 * t - 1.70158) end
function this.outBack(t)
  t = t - 1
  return (t * t * (2.70158 * t + 1.70158)) + 1
end
function this.inOutBack(t)
  t = t * 2
  if t < 1 then
    return 0.5 * (t * t * (3.5864016 * t - 2.5864016))
  else
    t = t - 2
    return 0.5 * (t * t * (3.5864016 * t + 2.5864016) + 2)
  end
end

function this.outBounce(t)
  if t < 1 / 2.75 then
    return 7.5625 * t * t
  elseif t < 2 / 2.75 then
    t = t - 1.5 / 2.75
    return 7.5625 * t * t + 0.75
  elseif t < 2.5 / 2.75 then
    t = t - 2.25 / 2.75
    return 7.5625 * t * t + 0.9375
  else
    t = t - 2.625 / 2.75
    return 7.5625 * t * t + 0.984375
  end
end
function this.inBounce(t) return 1 - this.outBounce(1 - t) end
function this.inOutBounce(t)
  if t < 0.5 then
    return this.inBounce(t * 2) * 0.5
  else
    return this.outBounce(t * 2 - 1) * 0.5 + 0.5
  end
end

function this.inSine(x)
  return 1 - cos(x * (pi * 0.5))
end

function this.outSine(x)
  return sin(x * (pi * 0.5))
end

function this.inOutSine(x)
  return 0.5 - 0.5 * cos(x * pi)
end

return this