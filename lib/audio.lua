local tick = require 'lib.tick'

sfx_volume = 1

sounds = {}
local current_volume = 1
local old_volume = 1
local sound_instances = {}

function registerSound(sound, volume)
  sounds[sound] = {
    data = love.sound.newSoundData(sound_path[sound]),
    volume = volume or 1
  }
end

function playSound(sound, volume, pitch)
  if sounds[sound] then
    if not sound_instances[sound] then
      sound_instances[sound] = 0
    end

    local source = love.audio.newSource(sounds[sound].data, "static")

    local adjusted_volume = 1/(2^sound_instances[sound])
    source:setVolume((volume or 1) * adjusted_volume * sounds[sound].volume * sfx_volume)
    source:setPitch(pitch or 1)

    source:play()

    sound_instances[sound] = sound_instances[sound] + 1
    tick.delay(function() sound_instances[sound] = sound_instances[sound] - 1 end, sounds[sound].data:getDuration()/4)
  end
end